package br.com.itau.conta_corrente_oo.modelo;

public class ContaCorrente extends Conta {
	
	private double taxa = 0.05;
	
	
	@Override
	public boolean sacar(double valorSaque) {
		double valorTaxa = valorSaque * taxa;
		if (super.sacar(valorSaque + valorTaxa)) {
			subtrairTaxa(valorTaxa);
			return true;
		}
		return false;
	}

}
