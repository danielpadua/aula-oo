package br.com.itau.conta_corrente_oo.modelo;

public class Conta {
	private double saldo;
	
	public double getSaldo() {
		return this.saldo;
	}
	
	public void depositar(double valorDeposito) {
		this.saldo = this.saldo + valorDeposito;
	}
	
	public boolean sacar(double valorSaque) {
		if (this.saldo >= valorSaque) {
			this.saldo = this.saldo - valorSaque;
			return true;
		}
		return false;
	}
	
	protected void subtrairTaxa(double valorTaxa) {
		this.saldo = this.saldo - valorTaxa;
	}
}
