package br.com.itau.conta_corrente_oo;

import java.util.Scanner;

import br.com.itau.conta_corrente_oo.modelo.Conta;

/**
 * Hello world!
 *
 */
public class App 
{
	private static Conta conta;
	
    public static void main( String[] args )
    {
        Scanner scanner = new Scanner(System.in);
        conta = new Conta();
        
        System.out.println("Digite a operação desejada (S - Sacar / D - Depositar / Q - Sair)");        
        char r = scanner.next().toUpperCase().charAt(0);    	
    	loop: while (r != 'Q') {
    		switch (r) {
			case 'S':
				System.out.println("Digite o valor que deseja sacar:");
				double valorRetirar = scanner.nextDouble();
				if (conta.sacar(valorRetirar)) {
					System.out.println("Saque realizado com sucesso.");
				} else {
					System.out.println("Saldo insuficiente!");
				}
				break;
			case 'D':
				System.out.println("Digite o valor que deseja depositar:");
				double valorDepositar = scanner.nextDouble();
				conta.depositar(valorDepositar);
				break;
			case 'Q':	
				System.out.println("Você escolheu sair.");
				break loop;

			default:				
				System.out.println("Digite uma opção válida (S - Sacar / D - Depositar / Q - Sair).");
				break;
    		}
    		System.out.println("Seu saldo atual é de: " + conta.getSaldo());
    		System.out.println("Digite uma nova operação (S - Sacar / D - Depositar / Q - Sair)");
    		r = scanner.next().toUpperCase().charAt(0);
    	}
        System.out.println("Operações encerradas.");
    	System.out.println("Saldo final: " + conta.getSaldo());
    }
}
