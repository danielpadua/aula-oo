package br.com.itau.conta_corrente;

import java.util.Iterator;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
	static double saldo;
	
    public static void main( String[] args )
    {
    	Scanner scanner = new Scanner(System.in);
    	System.out.println("Digite a operação desejada (S - Sacar / D - Depositar / Q - Sair)");
    	
    	char r = scanner.next().toUpperCase().charAt(0);    	
    	loop: while (r != 'Q') {
    		switch (r) {
			case 'S':
				System.out.println("Digite o valor que deseja sacar:");
				double valorRetirar = scanner.nextDouble();
				if (valorRetirar <= saldo) {
					saldo = saldo - valorRetirar;
					System.out.println("Saque realizado com sucesso.");										
				} else {
					System.out.println("Saldo insuficiente!");
				}
				System.out.println("Seu saldo atual é de: " + saldo);
				break;
			case 'D':
				System.out.println("Digite o valor que deseja depositar:");
				double valorDepositar = scanner.nextDouble();
				saldo = saldo + valorDepositar;
				System.out.println("Seu saldo atual é de: " + saldo);
				break;
			case 'Q':	
				System.out.println("Você escolheu sair.");
				break loop;

			default:
				System.out.println("Digite uma opção válida (S - Sacar / D - Depositar / Q - Sair).");
				break;
			}
    		System.out.println("Digite uma nova operação (S - Sacar / D - Depositar / Q - Sair)");
    		r = scanner.next().toUpperCase().charAt(0);
    	}    	
    	System.out.println("Operações encerradas.");
    	System.out.println("Saldo final: " + saldo);
    }        
}
